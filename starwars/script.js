/**
 * Creador: Alex Nieto Mendez
 * Proyecto: Star Wars
 * Version: 1.0
 */

//Funcion que se ejecutará al cargar la pagina
function paginaReady() {
    ocultarFiltros();
    ocultarTabla();
    document.getElementById("filters_films").style.display = "block";

}

//Mostrará el div de filtros pasado por parametro
function mostrarFiltros(idFiltro) {
    ocultarFiltros();
    ocultarTabla();

    document.getElementById(idFiltro).style.display = "block";
}

/**
 * FUNCIONES QUE TRAERAN LOS DATOS
 * */

function getFilms() {
    let parametroAFiltrar = document.querySelector("input[name='radio_films']:checked").value;

    $.ajax({
        url: "https://swapi.dev/api/films/",
        success: function (result) {
            let data = result.results;
            filtrarPeliculas(data, parametroAFiltrar);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}

function getPlanets() {
    let parametroAFiltrar = document.querySelector("input[name='radio_planets']:checked").value;

    $.ajax({
        url: "https://swapi.dev/api/planets",
        success: function (result) {
            let data = result.results;
            filtrarPlanetas(data, parametroAFiltrar);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}

function getPeoples() {
    let parametroAFiltrar = document.querySelector("input[name='radio_people']:checked").value;

    $.ajax({
        url: "https://swapi.dev/api/people/",
        success: function (result) {
            let data = result.results;
            filtrarPersonajes(data, parametroAFiltrar);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}

function getSpecies() {

    $.ajax({
        url: "https://swapi.dev/api/species/",
        success: function (result) {
            let data = result.results;
            mostrarResultadoEspecies(data);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}

function getVehicles() {

    let parametroAFiltrar = document.querySelector("input[name='radio_vehicles']:checked").value;

    $.ajax({
        url: "https://swapi.dev/api/vehicles/",
        success: function (result) {
            let data = result.results;
            filtrarVehiculos(data, parametroAFiltrar);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}

function getStarships() {

    let parametroAFiltrar = document.querySelector("input[name='radio_starships']:checked").value;

    $.ajax({
        url: "https://swapi.dev/api/starships/",
        success: function (result) {
            let data = result.results;
            filtrarNaves(data, parametroAFiltrar);
        },
        error: function () {
            console.log("Error al cargar datos.");
        }
    });
}


/**
 * FILTROS
 * */

function filtrarPeliculas(data, parametro) {
    let resultado = data.sort(eval("ordenarPeliculas" + parametro));

    mostrarResultadoPeliculas(resultado);
}

function filtrarPlanetas(data, parametro) {
    let resultado = data.sort(eval("ordenarPlanetas" + parametro));

    mostrarResultadoPlanetas(resultado);
}

function filtrarPersonajes(data, parametro) {
    let resultado = data.sort(eval("ordenarPersonajes" + parametro));

    mostrarResultadoPersonajes(resultado);
}

function filtrarVehiculos(data, parametro) {
    let resultado = data.sort(eval("ordenarVehiculos" + parametro));

    mostrarResultadoVehiculos(resultado);
}

function filtrarNaves(data, parametro) {
    let resultado = data.sort(eval("ordenarNaves" + parametro));

    mostrarResultadoNaves(resultado);
}


/**
 * FUNCIONES QUE COMPARAN DATOS PARA ORDENARLOS
 * */

//Peliculas
function ordenarPeliculasDirector(a, b) {
    if (a.director > b.director) return 1;
    if (a.director < b.director) return -1;
    return 0;
}

function ordenarPeliculasTitulos(a, b) {
    if (a.title > b.title) return 1;
    if (a.title < b.title) return -1;
    return 0;
}

function ordenarPeliculasEpisodio(a, b) {
    if (a.episode_id > b.episode_id) return 1;
    if (a.episode_id < b.episode_id) return -1;
    return 0;
}

function ordenarPeliculasFecha(a, b) {
    if (new Date(a.release_date) > new Date(b.release_date)) return 1;
    if (new Date(a.release_date) < new Date(b.release_date)) return -1;
    return 0;
}

//Planetas
function ordenarPlanetasNombre(a, b) {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
}

function ordenarPlanetasPoblacion(a, b) {

    let p1 = parseInt(a.population);
    let p2 = parseInt(b.population);

    if (isNaN(p1) || isNaN(p2)) {
        if(isNaN(p1) && !isNaN(p2)) return 1;
        if(!isNaN(p1) && isNaN(p2)) return -1;
        return 0;
    } else {
        if (p1 > p2) return 1;
        if (p1 < p2) return -1;
        return 0;
    }

}

//Personajes
function ordenarPersonajesNombre(a, b) {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
}

function ordenarPersonajesGenero(a, b) {
    if (a.gender > b.gender) return 1;
    if (a.gender < b.gender) return -1;
    return 0;
}

function ordenarPersonajesAltura(a, b) {

    let p1 = parseInt(a.height);
    let p2 = parseInt(b.height);

    if (isNaN(p1) || isNaN(p2)) {
        if(isNaN(p1) && !isNaN(p2)) return 1;
        if(!isNaN(p1) && isNaN(p2)) return -1;
        return 0;
    } else {
        if (p1 > p2) return 1;
        if (p1 < p2) return -1;
        return 0;
    }

}

//Vehiculos
function ordenarVehiculosNombre(a, b) {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
}

function ordenarVehiculosYear_created(a, b) {
    let yearA = new Date(a.release_date).getFullYear();
    let yearB = new Date(b.release_date).getFullYear();

    if (yearA > yearB) return 1;
    if (yearA < yearB) return -1;
    return 0;
}

//Naves
function ordenarNavesNombre(a, b) {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
}

function ordenarNavesCoste(a, b) {
    let p1 = parseInt(a.cost_in_credits);
    let p2 = parseInt(b.cost_in_credits);

    if (isNaN(p1) || isNaN(p2)) {
        if(isNaN(p1) && !isNaN(p2)) return 1;
        if(!isNaN(p1) && isNaN(p2)) return -1;
        return 0;
    } else {
        if (p1 > p2) return 1;
        if (p1 < p2) return -1;
        return 0;
    }
}

function ordenarNavesPasajeros(a, b) {

    let p1 = parseInt(a.passengers);
    let p2 = parseInt(b.passengers);

    if (isNaN(p1) || isNaN(p2)) {
        if(isNaN(p1) && !isNaN(p2)) return 1;
        if(!isNaN(p1) && isNaN(p2)) return -1;
        return 0;
    } else {
        if (p1 > p2) return 1;
        if (p1 < p2) return -1;
        return 0;
    }

}


/**
 * FUNCIONES QUE GENERAN LA SALIDA 
 * */

function mostrarResultadoPeliculas(resultado) {
    ocultarFiltros();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "4");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Peliculas"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thTitulo = document.createElement("th");
    thTitulo.appendChild(document.createTextNode("Titulo"));
    fila2.appendChild(thTitulo);

    let thDirector = document.createElement("th");
    thDirector.appendChild(document.createTextNode("Director"));
    fila2.appendChild(thDirector);

    let thEpidosio = document.createElement("th");
    thEpidosio.appendChild(document.createTextNode("Epidosio"));
    fila2.appendChild(thEpidosio);

    let thFecha = document.createElement("th");
    thFecha.appendChild(document.createTextNode("Fecha"));
    fila2.appendChild(thFecha);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(film => {
        let tr = document.createElement("tr");

        let titulo = document.createElement("td");
        titulo.appendChild(document.createTextNode(film.title));
        tr.appendChild(titulo);

        let director = document.createElement("td");
        director.appendChild(document.createTextNode(film.director));
        tr.appendChild(director);

        let episodio = document.createElement("td");
        episodio.appendChild(document.createTextNode(film.episode_id));
        tr.appendChild(episodio);

        let fecha = document.createElement("td");
        fecha.appendChild(document.createTextNode(film.release_date));
        tr.appendChild(fecha);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);

}

function mostrarResultadoPlanetas(resultado) {
    ocultarFiltros();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "3");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Planetas"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thNombre = document.createElement("th");
    thNombre.appendChild(document.createTextNode("Nombre"));
    fila2.appendChild(thNombre);

    let thPoblacion = document.createElement("th");
    thPoblacion.appendChild(document.createTextNode("Poblacion"));
    fila2.appendChild(thPoblacion);

    let thClima = document.createElement("th");
    thClima.appendChild(document.createTextNode("Clima"));
    fila2.appendChild(thClima);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(planet => {
        let tr = document.createElement("tr");

        let nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(planet.name));
        tr.appendChild(nombre);

        let poblacion = document.createElement("td");
        poblacion.appendChild(document.createTextNode(planet.population));
        tr.appendChild(poblacion);

        let clima = document.createElement("td");
        clima.appendChild(document.createTextNode(planet.climate));
        tr.appendChild(clima);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);

}

function mostrarResultadoPersonajes(resultado) {
    ocultarFiltros();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "3");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Personajes"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thNombre = document.createElement("th");
    thNombre.appendChild(document.createTextNode("Nombre"));
    fila2.appendChild(thNombre);

    let thGenero = document.createElement("th");
    thGenero.appendChild(document.createTextNode("Genero"));
    fila2.appendChild(thGenero);

    let thAltura = document.createElement("th");
    thAltura.appendChild(document.createTextNode("Altura"));
    fila2.appendChild(thAltura);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(people => {
        let tr = document.createElement("tr");

        let nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(people.name));
        tr.appendChild(nombre);

        let genero = document.createElement("td");
        genero.appendChild(document.createTextNode(people.gender));
        tr.appendChild(genero);

        let altura = document.createElement("td");
        altura.appendChild(document.createTextNode(people.height));
        tr.appendChild(altura);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);
}

function mostrarResultadoEspecies(resultado) {
    ocultarFiltros();
    ocultarTabla();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "4");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Especies"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thNombre = document.createElement("th");
    thNombre.appendChild(document.createTextNode("Nombre"));
    fila2.appendChild(thNombre);

    let thLenguaje = document.createElement("th");
    thLenguaje.appendChild(document.createTextNode("Lenguaje"));
    fila2.appendChild(thLenguaje);

    let thDesignacion = document.createElement("th");
    thDesignacion.appendChild(document.createTextNode("Designacion"));
    fila2.appendChild(thDesignacion);

    let thClasificacion = document.createElement("th");
    thClasificacion.appendChild(document.createTextNode("Clasificacion"));
    fila2.appendChild(thClasificacion);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(specie => {
        let tr = document.createElement("tr");

        let nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(specie.name));
        tr.appendChild(nombre);

        let lenguaje = document.createElement("td");
        lenguaje.appendChild(document.createTextNode(specie.language));
        tr.appendChild(lenguaje);

        let designacion = document.createElement("td");
        designacion.appendChild(document.createTextNode(specie.designation));
        tr.appendChild(designacion);

        let clasificacion = document.createElement("td");
        clasificacion.appendChild(document.createTextNode(specie.classification));
        tr.appendChild(clasificacion);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);

}

function mostrarResultadoVehiculos(resultado) {
    ocultarFiltros();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "4");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Vehiculos"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thNombre = document.createElement("th");
    thNombre.appendChild(document.createTextNode("Nombre"));
    fila2.appendChild(thNombre);

    let thModelo = document.createElement("th");
    thModelo.appendChild(document.createTextNode("Modelo"));
    fila2.appendChild(thModelo);

    let thCapacidadCarga = document.createElement("th");
    thCapacidadCarga.appendChild(document.createTextNode("Capacidad de Carga"));
    fila2.appendChild(thCapacidadCarga);

    let thYear = document.createElement("th");
    thYear.appendChild(document.createTextNode("Año"));
    fila2.appendChild(thYear);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(vehicle => {
        let tr = document.createElement("tr");

        let nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(vehicle.name));
        tr.appendChild(nombre);

        let modelo = document.createElement("td");
        modelo.appendChild(document.createTextNode(vehicle.model));
        tr.appendChild(modelo);

        let capacidadCarga = document.createElement("td");
        capacidadCarga.appendChild(document.createTextNode(vehicle.cargo_capacity));
        tr.appendChild(capacidadCarga);

        let year = document.createElement("td");
        year.appendChild(document.createTextNode(new Date(vehicle.created).getFullYear()));
        tr.appendChild(year);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);
}

function mostrarResultadoNaves(resultado) {
    ocultarFiltros();

    let salida = document.getElementById("salida");
    salida.setAttribute("class", "w-100 pr-5 pl-5");

    let tabla = document.createElement("table");
    tabla.setAttribute("class", "table");

    let thead = document.createElement("thead");

    let fila1 = document.createElement("tr");
    let thPrincipal = document.createElement("th");
    thPrincipal.setAttribute("colspan", "4");
    thPrincipal.setAttribute("class", "text-center h4")
    thPrincipal.appendChild(document.createTextNode("Naves"));

    fila1.appendChild(thPrincipal);
    thead.appendChild(fila1);

    let fila2 = document.createElement("tr");

    let thNombre = document.createElement("th");
    thNombre.appendChild(document.createTextNode("Nombre"));
    fila2.appendChild(thNombre);

    let thModelo = document.createElement("th");
    thModelo.appendChild(document.createTextNode("Modelo"));
    fila2.appendChild(thModelo);

    let thCoste = document.createElement("th");
    thCoste.appendChild(document.createTextNode("Coste"));
    fila2.appendChild(thCoste);

    let thPasajeros = document.createElement("th");
    thPasajeros.appendChild(document.createTextNode("Pasajeros"));
    fila2.appendChild(thPasajeros);

    thead.appendChild(fila2);

    tabla.appendChild(thead);

    let tbody = document.createElement("tbody");

    resultado.forEach(starship => {
        let tr = document.createElement("tr");

        let nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(starship.name));
        tr.appendChild(nombre);

        let modelo = document.createElement("td");
        modelo.appendChild(document.createTextNode(starship.model));
        tr.appendChild(modelo);

        let coste = document.createElement("td");
        coste.appendChild(document.createTextNode(starship.cost_in_credits));
        tr.appendChild(coste);

        let passengers = document.createElement("td");
        passengers.appendChild(document.createTextNode(starship.passengers));
        tr.appendChild(passengers);

        tbody.appendChild(tr);
    });

    tabla.appendChild(tbody);

    salida.appendChild(tabla);
}

//Funcion que oculta todos los divs de filtros
function ocultarFiltros() {
    document.getElementById("filters_films").style.display = "none";
    document.getElementById("filters_planets").style.display = "none";
    document.getElementById("filters_people").style.display = "none";
    document.getElementById("filters_vehicles").style.display = "none";
    document.getElementById("filters_starships").style.display = "none";
}

//Oculta la tabla de salida
function ocultarTabla() {
    document.getElementById("salida").innerHTML = "";
    document.getElementById("salida").removeAttribute("class");
}